const hash = require('object-hash')
const getCollection = require('./lokiDB')

const errorHandler = function (error) {
    console.log('pickels=> error',error)
    return false
}

const addPhone = function(phone) {
    const newPhone = {
        type: phone.type,
        serial: phone.serial,
        color: phone.color,
        metadata: hash(phone.metadata || {meta:true})
    }
    return getCollection
        .then(collection => {
            collection.insert(newPhone)
            collection.saveToDb()
            return true
        })
        .catch(errorHandler)

}

const getAllPhones = function() {
     return getCollection
         .then(collection => collection.find({}))
         .catch(error => errorHandler(error))
}

const updatePhone = function(serial, phone) {
    return getCollection
        .then(collection => {
            const dbPhone = collection.findObject({'serial':serial})
            Object.assign(dbPhone,phone)
            collection.update(dbPhone)
            collection.saveToDb()
            return true
        })
        .catch(errorHandler)
}

const deletePhone = function(serial) {
    return getCollection
        .then(collection => {
            const dbPhone = collection.findObject({'serial':serial})
            collection.remove(dbPhone)
            collection.saveToDb()
            return true
        })
        .catch(errorHandler)
}

module.exports = {
    addPhone,
    getAllPhones,
    updatePhone,
    deletePhone
}