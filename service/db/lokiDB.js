const loki = require('lokijs')
const path = require('path')
const hash = require('object-hash')
const faker = require('faker')

const typeArray = require('./types')

let db, collection

const getCollection = new Promise((resolve, reject) => {
    if(collection) return resolve(collection)
    try {
        db = new loki(path.join(__dirname,'phone_db.json'),{
            autoload: true,
            autoloadCallback : databaseInitialize,
            autosave: true,
            autosaveInterval: 4000
        })

        function databaseInitialize() {
            collection = db.getCollection('phones')
            if(!collection) {
                collection = db.addCollection('phones',{ indices: ['serial']})
                collection.ensureUniqueIndex('serial');
                for(let i = 0 ; i < 100; i++ ) {
                    collection.insert({
                        serial: i.toString().padStart(5,'0') ,
                        type: faker.random.arrayElement(typeArray),
                        color: faker.commerce.color(),
                        metadata: hash({meta: true})})
                }
            }

            collection.saveToDb = function() { db.saveDatabase() }
            resolve(collection)
        }
    }
    catch (e) {
        reject(e)
    }
})

module.exports = getCollection.then(data => data)