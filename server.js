const express = require('express')
const server = express()
const bodyParser = require('body-parser')
const phoneRoutes = require('./routes/phones')


server.use(bodyParser.urlencoded({
    extended: true
}))

server.use(bodyParser.json())

server.use('/api/phones',phoneRoutes)

server.get('*',(req, res, next) => {
    res.send('nothing here')
})

server.listen(3000,() => console.log('pickels=> We app and running'))