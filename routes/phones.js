const express = require('express')
const router = express.Router()

const phones = require('../service/db/phones')

router.get('/',async (req, res, next) => {
    const phoneList = await phones.getAllPhones()
    if(phoneList) {
        res.json(phoneList)
    } else {
        res.status(400).send(`error`)
    }
})

router.post('/',async (req, res, next) => {
    const phone = req.body
    if(phone.hasOwnProperty('serial')) {
        const result = await phones.addPhone(phone)
        if(result) {
            res.status(202).send(`phone with serial: ${phone.serial} just created`)
        } else {
            res.status(400).send(`error`)
        }
    } else {
        res.status(200).send('no serial included')
    }
})

router.put('/:serial',async (req, res, next) => {
    const phone = req.body
    if(phone.hasOwnProperty('serial')) delete phone.serial

    const result = await phones.updatePhone(req.params.serial, phone)
    if(result) {
        res.status(202).send(`phone with serial: ${req.params.serial} updated`)
    } else {
        res.status(400).send(`error`)
    }
})

router.delete('/:serial',async (req, res, next) => {
    const result = await phones.deletePhone(req.params.serial)
    if(result) {
        res.status(202).send(`phone with serial: ${req.params.serial} deleted`)
    } else {
        res.status(400).send(`error`)
    }
})

module.exports = router